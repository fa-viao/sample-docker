<?php

$config = parse_ini_file('./config/app.ini');

$host = $config['host'];
$dbname = $config['dbname'];
$user = $config['user'];
$password = $config['password'];

//
$storage=$config['storage'];

// Attempt to connect to the database
try {
    $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
    // Set the error mode to exceptions
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "V2. V3 Connected to the $dbname database successfully.\r\n<br/>";
    // Generate file in a dedicated Path
    $req_dump = print_r($_REQUEST, true);
    $fp = file_put_contents($storage.'/my_dump.log', $req_dump, FILE_APPEND);
    ///

    echo "hostname: " . gethostname() . "\r\n<br/>";
    echo "nodename: " . $_ENV["NODENAME"] . "\r\n<br/>";

} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}


?>
