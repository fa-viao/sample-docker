#!/bin/sh
if [ -z $SNMP_COMMUNITY ]; then
  echo "please provide a value for SNMP_COMMUNITY env variable"
  exit 1
else
  if [ ! -f "/config/viao-snmpd.conf" ]; then
    ## Generate snmpd configuration file from template
    envsubst < /cfg_template/config.tpl > /config/viao-snmpd.conf
  fi

  cat /config/viao-snmpd.conf
  echo "starting service..."
  ## Manually launch SNMP (do not remove -C options !!! )
  /usr/sbin/snmpd -C -c /config/viao-snmpd.conf -f -Lo
fi