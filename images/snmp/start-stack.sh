#!/bin/bash

docker network create viao-snmpd
docker container create --name viao-server --network viao-snmpd \
    -e SNMP_COMMUNITY=viao-snmp \
    -v $(pwd)/prod_config:/config \
     snmpd:$1
docker container create --name cli --network viao-snmpd \
    -e SNMP_COMMUNITY=viao-snmp \
    -e SNMP_HOST=viao-server \
    snmpcli:$1

docker start viao-server
sleep 5
docker start cli


