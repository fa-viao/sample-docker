#!/bin/bash
if [ -z $1 ]; then
  echo "please provide a docker tag value (version)"
else
  docker build -f snmpd/Dockerfile -t snmpd:$1 snmpd
  docker build -f cli/Dockerfile -t snmpcli:$1 cli
fi
