#!/bin/sh
if [ -z $SNMP_COMMUNITY ]; then
  echo "please provide a value for SNMP_COMMUNITY env variable"
  exit 1
fi
if [ -z $SNMP_HOST ]; then
  echo "please provide a value for $SNMP_HOST env variable"
  exit 1
fi

while true
do
  echo "will query host: ${SNMP_HOST}, community ${SNMP_COMMUNITY}"
  snmpbulkwalk -v2c -c $SNMP_COMMUNITY $SNMP_HOST 1.3.6.1.2.1.2.2.1.10
  sleep 5
done