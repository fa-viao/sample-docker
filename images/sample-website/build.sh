#!/bin/bash
if [ -z $1 ]; then
  echo "please provide a docker tag value (version)"
else
  docker build -f image/Dockerfile -t website:$1 .
fi
