#!/bin/bash

versionTag=${1:-latest}
echo "building with tag ${versionTag}"
docker build -t python_app:${versionTag} .
