#!/bin/bash

versionTag=${1:-latest}
scripts/build.sh $versionTag
docker tag html_app:${versionTag} registry.gitlab.com/formation-docker1/sample-docker-2/fabhtmlapp:${versionTag}
docker tag html_app:${versionTag} registry.gitlab.com/formation-docker1/sample-docker-2/fabhtmlapp:latest


docker push registry.gitlab.com/formation-docker1/sample-docker-2/fabhtmlapp:${versionTag}
docker push registry.gitlab.com/formation-docker1/sample-docker-2/fabhtmlapp:latest