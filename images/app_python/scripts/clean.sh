#!/bin/bash

#cleanup
docker container stop db
docker container stop app_python
docker container rm db
docker container rm app_python
docker network rm python_app
