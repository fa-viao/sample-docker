#!/bin/bash
versionTag=${1:-latest}
scripts/build.sh ${versionTag}

#cleanup
docker container stop python_app
docker container stop python_app_db
docker network rm python_app

#create
docker network create python_app
docker run -d --network python_app --name db \
 -e MYSQL_USER=python \
 -e MYSQL_PASSWORD=python \
 -e MYSQL_ALLOW_EMPTY_PASSWORD=true \
 -e MYSQL_DATABASE=python \
 mysql
docker run -d --network python_app --name app_python -p 5010:5000 \
  -e DB_USERNAME=python \
  -e DB_PASSWORD=python \
  -e DB_PORT=3306 \
  -e DB_NAME=python \
  -e DB_URL=db \
  python_app
