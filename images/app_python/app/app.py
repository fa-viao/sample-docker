from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import text
import os

db = SQLAlchemy()
app = Flask(__name__)
username = os.getenv('DB_USERNAME')
password = os.getenv('DB_PASSWORD')
server_url  = os.getenv('DB_URL')
server_port = os.getenv('DB_PORT')
dbname = os.getenv('DB_NAME')

userpass = 'mysql+pymysql://' + username + ':' + password + '@'

app.config['SQLALCHEMY_DATABASE_URI'] = userpass + server_url + ':' + server_port + '/'+ dbname
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db.init_app(app)

@app.route('/')
def testdb():
    try:
        db.session.query(text('1')).from_statement(text('SELECT 1')).all()
        return '<html><body><h1>Formation Docker. Request Properly Handeled Mysql Connection Up to Date.</h1></body></html>'
    except Exception as e:
        error_text = "<p>The error:<br>" + str(e) + "</p>"
        hed = '<h1>Something is broken.</h1>'
        return hed + error_text

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)